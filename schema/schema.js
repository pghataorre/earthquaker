const {
  GraphQLObjectType, 
  GraphQLInt,
  GraphQLString,
  GraphQLBoolean,
  GraphQLList,
  GraphQLSchema
} = require('graphql');
const { baseUrl, baseRocketUrl } = require('../api/config');
const getApi = require('../api/api');

const LaunchType = new GraphQLObjectType({
  name: 'Launch',
  fields: () => ({
    flight_number: { type: GraphQLInt },
    mission_name: { type: GraphQLString },
    launch_year: { type: GraphQLString },
    launch_date_local: { type: GraphQLString },
    launch_success: { type: GraphQLBoolean },
    rocket: { type: RocketType }
  })
});


const RocketType = new GraphQLObjectType({
  name: 'Rocket',
  fields: () => ({
    id: { type: GraphQLInt },
    rocket_id: { type: GraphQLString },
    rocket_name: { type: GraphQLString },
    rocket_type: { type: GraphQLString }
  })
});

const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    rockets: {
      type: new GraphQLList(RocketType),
      resolve(parent, args) {
        return getApi(baseRocketUrl);
      }
    },
    rocket: {
      type: RocketType,
      args: {
      id: {type: GraphQLInt}
      },
      resolve(parent, args) {
        return getApi(`${baseRocketUrl}/${rocket_number}`);
      }
    },
    launches: {
      type: new GraphQLList(LaunchType),
      resolve(parent, args) {
        return getApi(baseUrl);
      }
    }, 
    launch: {
      type: LaunchType,
      args: {
        flight_number: {type: GraphQLInt}
      },
      resolve(parent, args){
        const flight_number = args.flight_number || 0;
        return getApi(`${baseUrl}/${flight_number}`);
      }
    }
  }
});

module.exports = new GraphQLSchema({
  query: RootQuery
});
