import React, { Fragment } from 'react';
import Moment from 'react-moment';

function Error(props) {
  return (
    <Fragment>
      <Moment parse="DD-MM-YYYY HH:mm">{props.date}</Moment>
    </Fragment>
  );
}

export default Error;
