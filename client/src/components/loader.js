import React, { Fragment } from 'react';

function Loader() {
  return (
    <Fragment>
     <h4>LOADING . . .</h4>
    </Fragment>
  );
}

export default Loader;
