import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import Loader from './loader';
import Error from './error';
import DateItem from './dateItem';
import MissionKey from './missionKey';

const LAUNCHES_QUERTY = gql`
  query LaunhesQuery {
    launches {
      flight_number
      mission_name
      launch_date_local
      launch_success
    }
  }
`;

function Launches() {
  const missionSuccessClassNames = (launch_success) => {
    return launch_success ? 'text-success' : 'text-danger'
  }

  const LaunchItems = (data, index) => {
    return data.launches.map((datasetItem, index) => {

      let {flight_number, mission_name, launch_date_local, launch_success } = datasetItem;
      let linkItem = `/launchDetails/${flight_number}`

      return <div className="card card-body mb-3" key={index}>
        <div className="row">
          <div className="col-md-8">
            <h4>Mission: <span className={missionSuccessClassNames(launch_success)}>{mission_name}</span></h4>
            <h6>Date: <DateItem date={launch_date_local} /></h6>
          </div>
          <div className="col-md-4">
            <Link className="btn btn-secondary" to={linkItem}>Launch Details</Link>
          </div>
        </div>
      </div>
    });
  }

  return (
    <Fragment>
      <h3 className="display-4 my-3">Launches</h3>
      <MissionKey />
      <Query query={ LAUNCHES_QUERTY }>
        {
          ({ loading, error, data }) => {
            if (loading) return <Loader />;
            if (error) return <Error />;
           
            return LaunchItems(data);
          }
        }
      </Query>
    </Fragment>
  );
}

export default Launches;
