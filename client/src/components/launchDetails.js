import React, { Fragment } from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import Loader from './loader';
import Error from './error';
import DateItem from './dateItem';
import { Link } from 'react-router-dom';

const LAUNCH_QUERY = gql`
query LaunchQuery($flight_number: Int!) {
  launch(flight_number: $flight_number) {
    flight_number
    mission_name
    launch_year
    launch_success
    launch_date_local
    rocket {
      rocket_id
      rocket_name
      rocket_type
    }
  }
}
`;

function LaunchDetails(props) {
  const flight_number = parseInt(props.match.params.flight_number);

  const missionSuccessClassNames = (launch_success) => {
    return launch_success ? 'text-success' : 'text-danger'
  }

  const LaunchData = (data) => {
    let { flight_number, mission_name, launch_year, launch_success, launch_date_local, rocket } = data.launch;
    let {rocket_id, rocket_name, rocket_type} = rocket;

    return <div className="mb-6">
      <h5 className="display-6 my-3 mb-4">
        Mission: <span className="text-dark">{mission_name}</span>
      </h5>
      <ul className="list-group">
        <li className="list-group-item">Flight number: {flight_number}</li>
        <li className="list-group-item">Lauch Year: {launch_year}</li>
        <li className="list-group-item">Launch Date: <DateItem date={launch_date_local}/></li>
        <li className="list-group-item">Launch Success: <span className={missionSuccessClassNames(launch_success)}>{launch_success ? 'LAUNCH SUCCESSFUL' : 'LAUNCH FAILED'}</span></li>
      </ul>
      <h5 className="display-6 my-3 mb-4">
        Rocket Details: <span className="text-dark">{rocket_name}</span>
      </h5>
      <ul className="list-group">
        <li className="list-group-item">Rocket Number: {rocket_id}</li>
        <li className="list-group-item">Rocket Name: {rocket_name}</li>
        <li className="list-group-item">Rocket Type : {rocket_type}</li>
      </ul>
    </div>
  } 

  return (
    <Fragment>
      <h3>Launch Details</h3>
      <Query query={ LAUNCH_QUERY } variables={{flight_number}}>
        {
          ({ loading, error, data }) => {
            if (loading) return <Loader />;
            if (error) return <Error />;

            return LaunchData(data);
          }
        } 
      </Query>
      <div className="mb-6">
        <Link to="/" className="btn btn-secondary">Back</Link>
      </div>
    </Fragment>
  );
}

export default LaunchDetails;
