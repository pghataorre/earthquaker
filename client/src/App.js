import React from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import { BrowserRouter  as Router, Route } from 'react-router-dom';
import Launches from './components/launches';
import LaunchDetails from './components/launchDetails';
import './App.css';

const client = new ApolloClient({
  uri: '/graphql'
});


function App() {
  return (
    <ApolloProvider client={client}>
      <Router>
        <div className="App container">
          <h1>TEST APP</h1>
          <Route exact path="/" component={Launches} />
          <Route path="/launchDetails/:flight_number" component={LaunchDetails} />
        </div>
      </Router>
    </ApolloProvider>
  );
}

export default App;
