const apiConfig = {
  baseUrl: 'https://api.spacexdata.com/v3/launches',
  baseRocketUrl: 'https://api.spacexdata.com/v3/rockets'
}

module.exports = apiConfig;
