const axios = require('axios');

const getApi = (url) => {
  return axios.get(url)
    .then((response) => {
      return response.data;
    }).catch((err) => {
      console.log(`=========================================== ERROR = \n ${err} \n===========================================`);
    return {error: err};
  });
}

module.exports = getApi;

